# Arif Uz Zaman // CSE,BUET // 1005031

listString = []

def printASCII(str1):
    print ''.join(str(ord(c)) for c in str1) # ord() function convert String to ASCII value and put value in a list
    
while True:
    newString = raw_input("\nEnter String (Maximum Length 5) : ")
    if newString == '$':
        break
    
    length = len(newString)
    if length>5:
        print "Maximum Allowed String Length 5.Please Try Again."
        continue
    
    listString.append(newString)

for i in range(0,len(listString)):
    printASCII(listString[i])